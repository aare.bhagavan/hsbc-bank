import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TransctionsComponent } from './transctions.component';


const routes: Routes = [
  {
    path: '',
    component : TransctionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransctionsRoutingModule { }

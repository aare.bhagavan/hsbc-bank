import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransctionsRoutingModule } from './transctions-routing.module';
import { TransctionsComponent } from './transctions.component';


@NgModule({
  declarations: [TransctionsComponent],
  imports: [
    CommonModule,
    TransctionsRoutingModule
  ]
})
export class TransctionsModule { }

import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-allpayees',
  templateUrl: './allpayees.component.html',
  styleUrls: ['./allpayees.component.scss']
})
export class AllpayeesComponent implements OnInit {
items: any;
  constructor(
    private appService: AppService,
    public router: Router
  ) { }

  ngOnInit(): void {
   this. getAllpayeers();
  }
  getAllpayeers() {
    try {
      this.appService.getMethod('getAllpayeers').subscribe(
        (resp: any) => {
          if (resp.success) {
            this.items = resp.data;
            console.log(resp);
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
  payee(event, data) {
    this.router.navigate(['/sendmoney'], data);
}
}



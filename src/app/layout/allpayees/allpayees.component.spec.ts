import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllpayeesComponent } from './allpayees.component';

describe('AllpayeesComponent', () => {
  let component: AllpayeesComponent;
  let fixture: ComponentFixture<AllpayeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllpayeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllpayeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

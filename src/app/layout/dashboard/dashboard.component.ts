import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  adminData: any;
  userData: any = {};
  constructor(
    private cookieService: CookieService,
    private appService: AppService,
    public router: Router
  ) {

    if (this.cookieService.get('adminData')) {
      this.adminData = JSON.parse(this.cookieService.get('adminData'));
    }

  }
  ngOnInit() {
    this.getuserData();
  }
  getuserData() {
    try {
      const data = {
        account_number: this.adminData.account_number
      };
      this.appService.postMethod('getusers', data)
        .subscribe((resp: any) => {
          if (resp.success) {
            console.log(resp);
            this.userData = resp.data;
          } else {
            swal.fire(resp.msg, 'Something went wrong!', 'error');
          }
        },
          error => {
          });
    } catch (e) { }
  }
}

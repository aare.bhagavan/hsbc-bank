import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherpayeeComponent } from './otherpayee.component';

describe('OtherpayeeComponent', () => {
  let component: OtherpayeeComponent;
  let fixture: ComponentFixture<OtherpayeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherpayeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherpayeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

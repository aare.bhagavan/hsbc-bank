import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OtherpayeeComponent } from './otherpayee.component';


const routes: Routes = [
  {
    path: '', 
    component: OtherpayeeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OtherpayeeRoutingModule { }

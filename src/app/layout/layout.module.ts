import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { NavComponent } from './nav/nav.component';
// import { PayrollSheetComponent } from './payroll-sheet/payroll-sheet.component';
// import { AddEmployeeComponent } from './add-employee/add-employee.component';
// import { AddEmployeeComponent } from './add-employee/add-employee.component';
// import { AddEmployeesComponent } from './add-employees/add-employees.component';
// import { PushNotificationsComponent } from './push-notifications/push-notifications.component';
// import { LocalReportsComponent } from './local-reports/local-reports.component';


@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        TranslateModule
    ],
    declarations: [LayoutComponent, NavComponent, TopnavComponent, SidebarComponent, 
        // PayrollSheetComponent, 
        // AddEmployeeComponent, 
        // AddEmployeesComponent,  
     ]

})
export class LayoutModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },  
            {
                path: 'transctions',
                loadChildren: () => import('./transctions/transctions.module').then(m => m.TransctionsModule)
            }, 
            {
                path: 'allpayees',
                loadChildren: () => import('./allpayees/allpayees.module').then(m => m.AllpayeesModule)
            },  
            // {
            //     path: 'offers',
            //     loadChildren: () => import('./offers/offers.module').then(m => m.OffersModule)
            // },  
            {
                path: 'payments',
                loadChildren: () => import('./payments/payments.module').then(m => m.PaymentsModule)
            }, 
            {
                path: 'addpayee',
                loadChildren: () => import('./addpayee/addpayee.module').then(m => m.AddpayeeModule)
            }, 
            {
                path: 'sendmoney',
                loadChildren: () => import('./sendmoney/sendmoney.module').then(m => m.SendmoneyModule)
            }, 
            {
                path: 'otherpayee',
                loadChildren: () => import('./otherpayee/otherpayee.module').then(m => m.OtherpayeeModule)
            },  
           
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}

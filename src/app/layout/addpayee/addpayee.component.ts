import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from 'src/app/app.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-addpayee',
  templateUrl: './addpayee.component.html',
  styleUrls: ['./addpayee.component.scss']
})
export class AddpayeeComponent implements OnInit {
  Normalform = new FormGroup({});
  sender: any = {};
  adminData: any;
  paramsObj: any = {};
  constructor(
    private cookieService: CookieService,
    private appService: AppService,
    public router: Router
  ) {
  }

  ngOnInit(): void {
  }
  saveData() {
    try {
      // this.paramsObj['name'] = this.sender.name;
      // this.paramsObj['mobile_number'] = this.sender.mobile_number;
      // this.paramsObj['account_number'] = this.sender.account_number;
      // this.paramsObj['confirm_account_number'] = this.sender.confirm_account_number;
      // this.paramsObj['ifsccode'] = this.sender.ifsccode;
      this.appService.postMethod('addpayee', this.sender).subscribe(
        (resp: any) => {
          if (resp.success) {
            this.sender = {};
            swal.fire(resp.msg, 'success');
          } else {
            swal.fire(resp.msg, 'error');
          }
        },
        (error) => { }
      );
    } catch (e) { }
  }
}






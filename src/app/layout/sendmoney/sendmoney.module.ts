import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SendmoneyRoutingModule } from './sendmoney-routing.module';
import { SendmoneyComponent } from './sendmoney.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [SendmoneyComponent],
  imports: [
    CommonModule,
    SendmoneyRoutingModule,
    FormsModule
  ]
})
export class SendmoneyModule { }

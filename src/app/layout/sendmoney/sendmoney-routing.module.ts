import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SendmoneyComponent } from './sendmoney.component';


const routes: Routes = [
  {
    path: '',
    component : SendmoneyComponent
  },
  {
    path: ':account_number',
    component: SendmoneyComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SendmoneyRoutingModule { }

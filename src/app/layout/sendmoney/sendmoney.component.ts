import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd, Params } from '@angular/router';
import { AppService } from 'src/app/app.service';
import swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
@Component({
  selector: 'app-sendmoney',
  templateUrl: './sendmoney.component.html',
  styleUrls: ['./sendmoney.component.scss']
})
export class SendmoneyComponent implements OnInit {
  adminData: any  = {};
  userData: any = {};
  filter: any = {};
  accountuserData: any = {};

  Normalform = new FormGroup({});
  constructor(private route: ActivatedRoute,
    private appService: AppService,
    private cookieService: CookieService) {
      if (this.cookieService.get('adminData')) {
        this.accountuserData = JSON.parse(this.cookieService.get('adminData'));
      }
  }

  ngOnInit(): void {
    this.filter.type = 'NEFT';
    // tslint:disable-next-line:no-shadowed-variable
    const Params = this.route.snapshot.params;
    this.adminData = Params;
    // console.log(this.accountuserData);
    this.getpayee();
  }
  getpayee() {
    try {
      const data = {
        account_number: this.adminData.account_number
      };
      this.appService.postMethod('getpayuser', data)
        .subscribe((resp: any) => {
          if (resp.success) {
            // console.log(resp);
            this.userData = resp.data;
          } else {
            swal.fire(resp.msg, 'Something went wrong!', 'error');
          }
        },
          error => {
          });
    } catch (e) { }
  }
  sendmoney() {
    try {
    const dataObj = {};
    dataObj['name'] = this.accountuserData.name;
    dataObj['mobile_number'] = this.userData.mobile_number;
    dataObj['email'] = this.accountuserData.email;
    dataObj['account_number'] = this.accountuserData.account_number;
    dataObj['ifsccode'] = this.accountuserData.ifsccode;
    dataObj['payeer_name'] = this.userData.name;
    dataObj['payeer_email'] = this.userData.email;
    dataObj['payeer_account_number'] = this.userData.account_number;
    dataObj['payeer_ifsccode'] = this.userData.ifsccode;
    dataObj['branch'] = this.userData.branch;
    dataObj['amount'] = this.filter.amount;
    dataObj['remark'] = this.filter.remark;
    dataObj['type'] = this.filter.type;
    console.log(dataObj);
      this.appService.postMethod('addTransactions', dataObj)
        .subscribe((resp: any) => {
          if (resp.success) {
            console.log(resp);
          } else {
            swal.fire(resp.msg, 'Something went wrong!', 'error');
          }
        },
          error => {
          });
    } catch (e) { }
  }
}




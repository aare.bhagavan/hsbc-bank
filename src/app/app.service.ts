import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { DOCUMENT } from '@angular/common';
@Injectable({
    providedIn: 'root'
})
export class AppService {
    constructor(
        private httpService: HttpClient,
        @Inject(DOCUMENT) private document: any,
        private cookieService: CookieService
    ) { }
    Upload_Url: string = this.getURL(2);

    public readonly Url = this.getURL(1);
    Display_Type: { [s: string]: unknown } | ArrayLike<unknown>;
    public readonly ImageUpload_Url = this.getURL(2);
    public readonly ImageUrl = this.getURL(2);

    navclickins = [
        {
            navIte: 'User',
            link: 'user',
            isVisiable: false
        },

        {
            navIte: 'News Articles',
            link: 'Article_News',
            isVisiable: false
        },
        {
            navIte: 'Categories',
            link: 'categories',
            isVisiable: false
        },
        {
            navIte: 'Help',
            link: 'help',
            isVisiable: false
        },
        {
            navIte: 'Advertisement',
            link: 'Advertisement',
            isVisiable: false
        },
        {
            navIte: 'Employee',
            link: 'Employee',
            isVisiable: false
        },
        {
            navIte: 'Static Data',
            link: 'static-data',
            isVisiable: false
        },

        {
            navIte: 'Admin',
            link: 'Admin',
            isVisiable: false
        }
    ];
    getURL(Type) {
        if (Type === 1) {
            if (this.document.location.hostname === 'admin.anynews.co.in') {
                return 'https://api.anynews.co.in/admin/';
            } else {
                return 'http://localhost:3000/';
            }
        } else if (Type === 2) {
            if (this.document.location.hostname === 'admin.anynews.co.in') {
                return 'https://api.anynews.co.in/upload/';
            } else {
                return 'http://localhost:3000/';
            }
        }
    }
    getMethod(posturl: string): any {
        return this.httpService.get(this.Url + posturl);
    }
    getMethods(posturl: string, body): any {
        return this.httpService.get(this.Url + posturl, body);
    }
    postMethod(posturl: string, body: any): any {
        return this.httpService.post(this.Url + posturl, body);
    }
    postMethodImage(posturl: string, body: any): any {
        return this.httpService.post(this.ImageUrl + posturl, body);
    }
    onUploadFile(req): Observable<any> {
        return this.httpService.request(req);
    }
    Event_Booking_Type(Event_Booking_Type: any) {
        throw new Error('Method not implemented.');
    }
    getLanguageList() {
        throw new Error('Method not implemented.');
    }
    // getLanguageList() {
    // throw new Error('Method not implemented.');
    // }
    checkUploadingStatus(arg0: string, body: { VideoID: any }) {
        throw new Error('Method not implemented.');
    }
    Jaggu_postMethod(
        arg0: string,
        body: {
            AdminID: any;
            SessionID: any;
            Advertisement_Name: any;
            Advertisement_Link: any;
            Description: any;
            Advertisement_Type: any;
            Company_Name: any;
            SNo: any;
            AreaCode_Array: any;
        }
    ) {
        throw new Error('Method not implemented.');
    }
    getNavLinkItesm() {
        const adminData: any = JSON.parse(this.cookieService.get('adminData'));
        if (adminData.Permissions.Admin_Section_Permision) {
            this.navclickins.forEach(item => {
                item.isVisiable = true;
            });
            return this.navclickins;
        } else {
            if (adminData.Employee_Permissions === undefined) {
                this.navclickins.forEach(item => {
                    item.isVisiable = true;
                });
                return this.navclickins;
            } else {
                let permistions = [];
                permistions = Object.keys(
                    adminData.Employee_Permissions
                ).filter(k => adminData.Employee_Permissions[k]);
                this.navclickins.forEach(item => {
                    permistions.forEach(subindex => {
                        if (subindex === item.navIte) {
                            item.isVisiable = true;
                        }
                    });
                });
                return this.navclickins;
            }
        }
    }
}

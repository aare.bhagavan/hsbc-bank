import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { EmployeeComponent } from './layout/employee/employee.component';
import { AuthGuard } from './shared/guard/auth.guard';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule),
        canActivate: [AuthGuard]
    },
    {
        path: 'login',
        loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
    },
    {
            path: 'offers',
            loadChildren: () => import('./offers/offers.module').then(m => m.OffersModule)
        },  
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [AuthGuard],
    entryComponents:[]
})
export class AppRoutingModule {}
